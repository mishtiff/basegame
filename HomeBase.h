#pragma once

#include "GameObject.h"

class HomeBase : public GameObject
{
private:
	int lives;
	int score;
	int animationRow;
	float spin;
	bool attacking;

public:
	HomeBase();
	void Destroy();

	void Init(ALLEGRO_BITMAP *image = NULL);
	void Update();
	void Render();
	
	void ResetAnimation(int position);
	void setSpin(){spin += .025; if(spin > 359)spin = 0;}
	float getSpin(){return spin;}

	int GetLives() {return lives;}
	void setScore(int i) {score += i;}
	int GetScore() {return score;}
	int GetAttacking() {return attacking;}
		
	void LoseLife() {lives--;}
	void AddPoint() {score++;}

	void Collided(int objectID, float velx, float vely);
};