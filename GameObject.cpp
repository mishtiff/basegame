#include "GameObject.h"

GameObject::GameObject()
{
	ID = 0;
	PlayerID = 0;
	
	font18 = al_load_font("arial.ttf", 18, 0);
	velX = 0;
	velY = 0;

	dirX = 0;
	dirY = 0;

	boundX = 0;
	boundY = 0;
	
	shipCount = 0;
	shipKiller = 0;

	maxFrame = 0;
	curFrame = 0;
	frameCount = 0;
	frameDelay = 0;
	frameWidth = 0;
	frameHeight = 0;
	animationColumns = 0;
	animationDirection = 0;

	image = NULL;
	BaseColor = al_map_rgb(255,255,255);

	alive = true;
	collidable = true;
}

void GameObject::Destroy()
{
	al_destroy_font(font18);
	al_destroy_bitmap(image);
}

void GameObject::Init(Vec2 pos, float velX, float velY, int dirX, int dirY, int boundX, int boundY)
{
	GameObject::position = pos;

	GameObject::velX = velX;
	GameObject::velY = velY;

	GameObject::dirX = dirX;
	GameObject::dirY = dirY;

	GameObject::boundX = boundX;
	GameObject::boundY = boundY;
}

void GameObject::Update()
{}

void GameObject::Render()
{}

bool GameObject::CheckCollisions(GameObject *otherObject)
{
	Vec2 other_pos = otherObject->GetPosition();

	float diffx = (other_pos.x + otherObject->GetBoundX()) - (position.x + boundX);
	float diffy = (other_pos.y + otherObject->GetBoundY()) - (position.y + boundY);

	float distance = (diffx * diffx + diffy + diffy) * (diffx * diffx + diffy + diffy);

	if( distance < boundX + otherObject->GetBoundX())
		return true;
	else
		return false;
}

void GameObject::Collided(int objectID, Vec2 passed_position)
{}

bool GameObject::Collidable()
{
	return alive && collidable;
}
