#pragma once

#include "GameObject.h"

class Ship : public GameObject
{
private:
	int health;
	int attack;
	int animationRow;
	ALLEGRO_COLOR BaseColor;

protected:
	bool moving;
	float speed;
	float range;

public:
	Ship();
	void Destroy();

	void Init(ALLEGRO_BITMAP *image = NULL);
	void Update();
	void Render();

	void MoveUp();
	void MoveDown();
	void MoveLeft();
	void MoveRight();
	int AttackMove(int dirx, int diry){position.x += dirx; position.y += diry;};

	void ResetAnimation(int position);
	
	void setHealth(int hlth) {Ship::health += hlth; if(getHealth() <= 0) SetAlive(false);}
	int getHealth() {return health;}

	void setMoving(bool move) {moving = move;}
	bool getMoving() {return moving;}

	void setSpeed(float speed) {Ship::speed += speed;}
	float getSpeed() {return speed;}

	void setAttack(int atk) {Ship::attack += atk;}
	int getAttack() {return attack;}

	void setRange(float rng) {Ship::range += rng;}
	float getRange() {return range;}
	
	void SetColor(ALLEGRO_COLOR color) {BaseColor = color;}
	ALLEGRO_COLOR GetColor() {return BaseColor;}
	
	void Collided(int objectID, Vec2 passed_position);
	bool Attackble(Ship *otherShip);
};