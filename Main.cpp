#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <list>
#include <fstream>
#include <string>
#include <sstream>
#include <iostream>
#include <boost/geometry/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/geometries/box.hpp>
#include <boost/geometry/geometries/register/point.hpp>
#include <boost/geometry/geometries/register/box.hpp>
#include <boost/geometry/index/rtree.hpp>

#include "GameObject.h"
#include "Ship.h"
#include "Bullet.h"
#include "HomeBase.h"
#include "Globals.h"

bool keys[] = {false, false, false, false, false, false};
enum KEYS{W, S, A, D, SPACE, RCLICK};



//globals
//SpaceShip *ship;
std::list<GameObject *> objects;
std::list<GameObject *>::iterator iter;
std::list<GameObject *>::iterator iter2;
std::list<HomeBase *> Bases;
std::list<HomeBase *>::iterator Baseiter;
std::list<HomeBase *>::iterator Baseiter2;
std::list<Ship *> ships;
std::list<Ship *>::iterator Shipiter;
std::list<Ship *>::iterator Shipiter2;
float mousex, mousey;

//ALLEGRO_SAMPLE_INSTANCE *songInstance;

//prototypes
void ChangeState(STATE &state, STATE newState);



BOOST_GEOMETRY_REGISTER_POINT_2D(Vec2,float,cs::cartesian,x,y);
BOOST_GEOMETRY_REGISTER_BOX(Rect,Vec2,topLeft,bottomRight);
namespace bgi = boost::geometry::index;

	
struct AreRTreeValuesEqual
  {
     bool operator()(GameObject* v1, GameObject* v2) const
     {
        return v1 == v2;
     }
  };
struct GetRTreeIndexable
 {
    typedef const Rect& result_type;
   result_type operator()(GameObject* v) const
    {
       return v->GetBoundingBox();
    }
 };

//8 is the number of actors per node. you could try and specify a different number.
typedef bgi::rtree<GameObject*, bgi::quadratic<20>, GetRTreeIndexable, AreRTreeValuesEqual> RTree;

void GameLogic(bool &render, STATE state, int &spawner, RTree *pRTree);
void GameRender(bool &render, int &frames, STATE state, ALLEGRO_FONT *font18, int gameFPS);
void spawnShips();
void upgradeShips();
void updateObjects();
void Collisions(RTree *pRTree);
void cullTheDead();

int main(int argc, char **argv)
{
	//==============================================
	//SHELL VARIABLES
	//==============================================
	bool done = false;
	bool render = false;
	bool gameLogic = false;

	float gameTime = 0;
	int frames = 0;
	int gameFPS = 0;
	int spawner = 0;


	//==============================================
	//PROJECT VARIABLES
	//==============================================
	STATE state = PLAYING;
	RTree* pRTree = NULL;
		

	//==============================================
	//ALLEGRO VARIABLES
	//==============================================
	ALLEGRO_DISPLAY *display = NULL;
	ALLEGRO_EVENT_QUEUE *event_queue = NULL;
	ALLEGRO_TIMER *timer;
	ALLEGRO_FONT *font18;
	
	//==============================================
	//ALLEGRO INIT FUNCTIONS
	//==============================================
	if(!al_init())										//initialize Allegro
		return -1;

	display = al_create_display(WIDTH, HEIGHT);			//create our display object

	if(!display)										//test display object
		return -1;

	//==============================================
	//ADDON INSTALL
	//==============================================
	al_install_keyboard();
	al_install_mouse();
	al_init_image_addon();
	al_init_font_addon();
	al_init_ttf_addon();
	al_init_primitives_addon();
	al_install_audio();
	al_init_acodec_addon();


	//==============================================
	//PROJECT INIT
	//==============================================
	font18 = al_load_font("arial.ttf", 18, 0);
	//al_reserve_samples(15);

	//Set Up bases
	Vec2 positions[10] = {Vec2 (WIDTH *.2, HEIGHT * .25), Vec2 (WIDTH *.4,HEIGHT * .25), Vec2 (WIDTH *.6,HEIGHT * .25), 
						  Vec2 (WIDTH *.8,HEIGHT * .25), Vec2 (WIDTH * .2,HEIGHT * .5), Vec2 (WIDTH * .4,HEIGHT * .5), Vec2 (WIDTH * .6,HEIGHT * .5), 
						  Vec2 (WIDTH * .8,HEIGHT * .5), Vec2 (WIDTH * .333333,HEIGHT * .75), Vec2 (WIDTH * .6666,HEIGHT * .75)};

	for(int i = 0; i != 10; i++)
	{
		HomeBase *Base = new HomeBase();
		Base->Init();
		Base->SetPlayerID(i+1);
		Base->SetPosition(positions[i]);
		Base->setBoundingBox(Base->GetPosition(), 3);
		if(i == 0)
			Base->SetColor(al_map_rgb(255,0,0));
		else if(i == 2)
			Base->SetColor(al_map_rgb(0,255,0));
		else if(i == 4)
			Base->SetColor(al_map_rgb(0,0,255));
		else if(i == 6)
			Base->SetColor(al_map_rgb(150,150,0));
		else if(i == 8)
			Base->SetColor(al_map_rgb(0,150,150));
		else
			Base->SetColor(al_map_rgb(255,255,255));
							
		Bases.push_back(Base);
		objects.push_back(Base);
	}	
	
	ChangeState(state, PLAYING); //change state to menu (playing for faster debugging)

	srand(time(NULL));

	//==============================================
	//TIMER INIT AND STARTUP
	//==============================================
	event_queue = al_create_event_queue();
	timer = al_create_timer(1.0 / 60);

	al_register_event_source(event_queue, al_get_timer_event_source(timer));
	al_register_event_source(event_queue, al_get_keyboard_event_source());
	al_register_event_source(event_queue, al_get_mouse_event_source());

	al_start_timer(timer);
	gameTime = al_current_time();
	while(!done)
	{

		while(!al_is_event_queue_empty(event_queue))
		{
			ALLEGRO_EVENT ev;
			al_get_next_event(event_queue, &ev);
			if(ev.type == ALLEGRO_EVENT_KEY_DOWN)
			{
				switch(ev.keyboard.keycode)
				{
				case ALLEGRO_KEY_ESCAPE:
					done = true;
					break;
				case ALLEGRO_KEY_A:
					keys[A] = true;
					break;
				case ALLEGRO_KEY_D:
					keys[D] = true;
					break;
				case ALLEGRO_KEY_W:
					keys[W] = true;
					break;
				case ALLEGRO_KEY_S:
					keys[S] = true;
					break;
				case ALLEGRO_KEY_SPACE:
					if(state == TITLE)
						ChangeState(state, PLAYING);
					else if(state == PLAYING)
					{
						keys[SPACE] = true;
					}
					else if(state == LOST)
						ChangeState(state, PLAYING);
					break;
				}
			}
			else if(ev.type == ALLEGRO_EVENT_KEY_UP)
			{
				switch(ev.keyboard.keycode)
				{
				case ALLEGRO_KEY_ESCAPE:
					done = true;
					break;
				case ALLEGRO_KEY_A:
					keys[A] = false;
					break;
				case ALLEGRO_KEY_D:
					keys[D] = false;
					break;
				case ALLEGRO_KEY_W:
					keys[W] = false;
					break;
				case ALLEGRO_KEY_S:
					keys[S] = false;
					break;
				case ALLEGRO_KEY_SPACE:
					keys[SPACE] = false;
					break;
				}
			}
			else if(ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
			{
				if(ev.mouse.button & 2)
				{
					keys[RCLICK] = true;
					mousex = ev.mouse.x;
					mousey = ev.mouse.y;
				}
			}
			else if(ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP)
			{
				keys[RCLICK] = false;
			}
			else if(ev.type == ALLEGRO_EVENT_TIMER)
			{
				gameLogic = true;
			}
		}

		if (gameLogic && al_is_event_queue_empty(event_queue)){
			//=====================
			//UPDATE FPS				
			//=====================
			if(al_current_time() - gameTime >= 1){
				gameTime = al_current_time();
				gameFPS = frames;
				frames = 0;}

			//std::cout << "Logic " << al_current_time() << std::endl;
			gameLogic = false;
			
			if (pRTree)
			delete pRTree;
			pRTree = new RTree(objects.begin(), objects.end());

			GameLogic(render, state, spawner, pRTree);
		}
				
		if(render){GameRender(render, frames, state, font18, gameFPS); //std::cout << "Render " << al_current_time() << std::endl;
		}


	}
	//==============================================
	//DESTROY PROJECT OBJECTS
	//==============================================
	for(iter = objects.begin(); iter != objects.end(); )
	{
		(*iter)->Destroy();
		delete (*iter);
		iter = objects.erase(iter);
	}
	ships.clear();


	//SHELL OBJECTS=================================
	al_destroy_font(font18);
	al_destroy_timer(timer);
	al_destroy_event_queue(event_queue);
	al_destroy_display(display);

	return 0;
	
}

void ChangeState(STATE &state, STATE newState)
{
	if(state == TITLE)
	{}
	else if(state == PLAYING)
	{
		for(iter = objects.begin(); iter != objects.end(); ++iter)
		{
			if( (*iter)->GetID() != SHIP && (*iter)->GetID() != BASE && (*iter)->GetID() != MISC)
				(*iter)->SetAlive(false);			
		}
	}
	else if(state == LOST)
	{}

	state = newState;

	if(state == TITLE)
	{}
	else if(state == PLAYING)
	{}
	else if(state == LOST)
	{}
}

void GameLogic(bool &render, STATE state, int &spawner, RTree *pRTree)
{
	render = true;

	if(state == PLAYING)
	{				
		spawner++;
		if (spawner >= 60){										
			spawnShips(); //spawns ships
			spawner = 0;}
							
		upgradeShips(); //upgrades ships
		updateObjects(); //update all objects
		Collisions(pRTree); //check collisions
		cullTheDead(); //remove dead ships
	}
}

void spawnShips()
{
	for(iter = objects.begin(); (*iter)->GetID() != SHIP; ++iter)
	{
		if((*iter)->GetID() != BASE) break;
		if((*iter)->GetShipCount() > 19) continue;

		Vec2 Base_pos = (*iter)->GetPosition();
		Ship *ship = new Ship();
		ship->Init();
		ship->SetPlayerID((*iter)->GetPlayerID());
		float randSpot = rand() % 360;
		float x = Base_pos.x + 30 * cos(randSpot);
		float y = Base_pos.y + 30 * sin(randSpot);
		ship->SetPosition(Vec2 (x, y));
		ship->setBoundingBox(ship->GetPosition(), 6);
		ship->SetColor((*iter)->GetColor());

		(*iter)->SetShipCount(1);
		objects.push_back(ship);
		ships.push_back(ship);
	}
}
void upgradeShips()
{
	if(keys[RCLICK]){ //moves ships. (includes mouse clicks)
		for(Shipiter = ships.begin(); Shipiter != ships.end(); ++Shipiter){
			if((*Shipiter)->GetPlayerID() != 1) continue;
			(*Shipiter)->SetDestX(mousex);
			(*Shipiter)->SetDestY(mousey);
			(*Shipiter)->setMoving(true);}}
	if(keys[W]){ //upgrades ships movement speed
		for(Shipiter = ships.begin(); Shipiter != ships.end(); ++Shipiter){
			if((*Shipiter)->GetPlayerID() != 1) continue;
			(*Shipiter)->setSpeed(.05);}}
				
	if(keys[S]){ //upgrade ships attack
		for(Shipiter = ships.begin(); Shipiter != ships.end(); ++Shipiter){
			if((*Shipiter)->GetPlayerID() != 1) continue;
			(*Shipiter)->setAttack(5);}}
					
	if(keys[A]){ //upgrades ships attack range
		for(Shipiter = ships.begin(); Shipiter != ships.end(); ++Shipiter){
			if((*Shipiter)->GetPlayerID() != 1) continue;
			(*Shipiter)->setRange(5);}}
					
	if(keys[D]){ //upgrades ship health
		for(Shipiter = ships.begin(); Shipiter != ships.end(); ++Shipiter){
			if((*Shipiter)->GetPlayerID() != 1) continue;
			(*Shipiter)->setHealth(5);}}
}
void updateObjects()
{
	for(iter = objects.begin(); iter != objects.end(); ++iter){
		(*iter)->Update();}}
void Collisions(RTree *pRTree)
{
	for(iter = objects.begin(); iter != objects.end(); ++iter)
		{
			if((*iter)->GetID() == BASE) continue;
			std::vector<GameObject*> collision;
			int count = 0;
			//std::vector<GameObject*> attackable;
			////used for nearest neighbor
			//pRTree->query(bgi::nearest((*iter)->GetPosition(), 1) && bgi::satisfies((*iter)->GetPlayerID(), std::back_inserter(attackable));
			/*if((*iter)->GetPlayerID() == 1)
				std::cout << attackable.size() << "||";*/
			//find all actors intersecting with the current one and insert results into the "results" vector:
			pRTree->query(bgi::intersects((*iter)->GetBoundingBox()),std::back_inserter(collision));
			count = collision.size();
			if(collision.size() > 1)
			{						
				for(int i = 0; i < count; i++)
				{
					if(collision[i] == (*iter)) continue;
					collision[i]->Collided((*iter)->GetID(), (*iter)->GetPosition());
				}
			}
			/*if(attackable.size() > 1)
			{						
				for(int i = 0; i < collision.size(); i++)
				{
					if(attackable[i] == (*iter)) continue;
							
				}
			}*/
		}
}

void cullTheDead()
{
	for(Shipiter = ships.begin(); Shipiter != ships.end(); )
		{
			if(!(*Shipiter)->GetAlive())
			{						
				Shipiter = ships.erase(Shipiter);
			}
			else
				Shipiter++;
		}
		for(iter = objects.begin(); iter != objects.end(); )
		{
			if(! (*iter)->GetAlive())
			{
				if((*iter)->GetID() == SHIP)
				{
					int id = (*iter)->GetPlayerID();
					for(Baseiter = Bases.begin(); Baseiter != Bases.end(); Baseiter++)
					{
						if((*Baseiter)->GetPlayerID() == id)
						{
							(*Baseiter)->SetShipCount(-1);
							int killer = (*iter)->GetShipKiller();
							for(Baseiter2 = Bases.begin(); Baseiter2 != Bases.end(); )
							{
								if((*Baseiter2)->GetPlayerID() == killer)
								{(*Baseiter2)->setScore(1); break;}
								else
									Baseiter2++;
							}
							break;
						}
					}

				}
				(*iter)->Destroy();
				delete (*iter);
				iter = objects.erase(iter);
						
			}
			else 
				iter++;
		}
}

void GameRender(bool &render, int &frames, STATE state, ALLEGRO_FONT *font18, int gameFPS)
{
	//std::cout << "Rendering" << std::endl;
	render = false;
	frames++;
			

	//BEGIN PROJECT RENDER================
	if(state ==TITLE)
	{}
	else if(state == PLAYING)
	{
		al_draw_textf(font18, al_map_rgb(255, 0, 255), 5, 35, 0, "FPS: %i   mouse: %f,%f", gameFPS, mousex, mousey);	//display FPS on screen
						
					

		for(iter = objects.begin(); iter != objects.end(); ++iter)
			(*iter)->Render();

	}
	else if(state == LOST)
	{}

	//FLIP BUFFERS========================
	al_flip_display();
	al_clear_to_color(al_map_rgb(0,0,0));
}

