#include "HomeBase.h"

HomeBase::HomeBase()
{}

void HomeBase::Destroy()
{
	GameObject::Destroy();
}

void HomeBase::Init(ALLEGRO_BITMAP *image)
{
	GameObject::Init(Vec2 (0, 0), 6, 6, 0, 0, 10, 12);

	GameObject::SetID(BASE);
	SetAlive(true);
	attacking = false;
	lives = 3;
	score = 0;
	spin = rand() % 180;

	maxFrame = 3;
	curFrame = 0;
	frameWidth = 46;
	frameHeight = 41;
	animationColumns = 3;
	animationDirection = 1;

	animationRow = 1;


	if(image != NULL)
		HomeBase::image = image;
}


void HomeBase::Update()
{
	GameObject::Update();
	setSpin();
}

void HomeBase::Render()
{
	GameObject::Render();

	al_draw_arc(position.x, position.y, 50, HomeBase::getSpin(), 5.85, HomeBase::GetColor(), 2);
	//al_draw_circle(position.x, position.y, 50, HomeBase::GetColor(), 2);
	al_draw_textf(font18, HomeBase::GetColor(), position.x - 20, position.y - 75, 0, "P%i: %i", GetPlayerID(), GetScore());

}

void HomeBase::Collided(int objectID, float velx, float vely)
{
	if(objectID == ENEMY)
		lives--;
}