#include "Ship.h"

Ship::Ship()
{}

void Ship::Destroy()
{
	GameObject::Destroy();
}

void Ship::Init(ALLEGRO_BITMAP *image)
{
	GameObject::Init(Vec2 (0,0), 0, 0, 0, 0, 3, 3);

	SetID(SHIP);
	SetAlive(true);

	health = 500;
	moving = false;
	speed = 1;
	attack = 1;
	range = 10;

	maxFrame = 3;
	curFrame = 0;
	frameWidth = 46;
	frameHeight = 41;
	animationColumns = 3;
	animationDirection = 1;

	animationRow = 1;
		
	BaseColor = al_map_rgb(255,255,255);

	if(image != NULL)
		Ship::image = image;
}


void Ship::Update()
{
	if(moving == true)
	{
		float distance = sqrt((desty - position.y) * (desty - position.y) + (destx - position.x) * (destx - position.x));
		float velx = getSpeed() * (destx - position.x) / distance;
		float vely = getSpeed() * (desty - position.y) / distance;
							
		SetVelX(velx);
		SetVelY(vely);

		Vec2 new_pos(velX, velY);
		SetPosition(new_pos);

		if(position.x < 0)
			position.x = 0;
		else if(position.x > WIDTH)
			position.x = WIDTH;

		if(position.y < 0)
			position.y = 0;
		else if(position.y > HEIGHT)
			position.y = HEIGHT;
	
		if(position.x - destx < 10 && position.x - destx > -10 && position.y - desty < 10 && position.y - desty > -10)
		{
			Ship::SetVelX(0);
			Ship::SetVelY(0);
			setMoving(false);
		}
	}
}

void Ship::MoveLeft()
{
	position.x -=  5;
}

void Ship::MoveRight()
{
	position.x +=  5;
}

void Ship::MoveDown()
{
	position.y +=  5;
}

void Ship::MoveUp()
{
	position.y -=  5;
}

void Ship::Render()
{
	//GameObject::Render();

	//
	//debugging options
	//
	//al_draw_rectangle(x - boundX, y - boundY, x + boundX, y + boundY, BaseColor, 1);
	//al_draw_circle(position.x, position.y, range, al_map_rgb(50,50,50), 1);


	al_draw_circle(position.x, position.y, boundX, BaseColor, 1);
}


void Ship::Collided(int objectID, Vec2 passed_position)
{
	if(objectID == SHIP)
	{
		float distance = sqrt((position.y - passed_position.y) * (position.y - passed_position.y) + (position.x - passed_position.x) * (position.x - passed_position.x));
		float velx = 3 * (position.x - passed_position.x) / distance;
		float vely = 3 * (position.y - passed_position.y) / distance;
		Vec2 move_to(velx, vely);
		Ship::SetPosition(move_to);
	}
}

bool Ship::Attackble(Ship *otherShip)
{
	Vec2 other_pos = otherShip->GetPosition();

	Vec2 diff = other_pos - GetPosition();
	float radii = getRange() + otherShip->GetBoundX();


	if((diff.x * diff.x) + (diff.y * diff.y) < radii * radii)
		return true;
	else
		return false;
}