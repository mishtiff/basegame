#pragma once

const int WIDTH = 1800;
const int HEIGHT = 900;

enum ID{SHIP, BASE, ENEMY, BULLET, BORDER, MISC, EXPLOSION};
enum SHIP{P1, P2, P3, P4, P5, P6, P7, P8, P9, P10};
enum STATE{TITLE, PLAYING, LOST};

struct Vec2
{
	float x,y;

	Vec2() { x = 0; y = 0; }
	Vec2(float a_x, float a_y) {x = a_x; y = a_y;}

	Vec2 operator+(const Vec2 &right) const
	{
		Vec2 ret;
		ret.x = x+right.x;
		ret.y = y+right.y;
		return ret;
	}
	Vec2 operator-(const Vec2 &right) const
	{
		Vec2 ret;
		ret.x = x-right.x;
		ret.y = y-right.y;
		return ret;
	}
	Vec2 operator*(const Vec2 &right) const
	{
		Vec2 ret;
		ret.x = x*right.x;
		ret.y = y*right.y;
		return ret;
	}
	Vec2 operator/(const Vec2 &right) const
	{
		Vec2 ret;
		ret.x = x/right.x;
		ret.y = y/right.y;
		return ret;
	}

	Vec2 operator+(float right) const
	{
		Vec2 ret;
		ret.x = ret.x + right;
		ret.y = ret.y + right;
		return ret;
	}
	Vec2 operator-(float right) const
	{
		Vec2 ret;
		ret.x = ret.x - right;
		ret.y = ret.y - right;
		return ret;
	}
	Vec2 operator*(float right) const
	{
		Vec2 ret;
		ret.x = ret.x * right;
		ret.y = ret.y * right;
		return ret;
	}
	Vec2 operator/(float right) const
	{
		Vec2 ret;
		ret.x = ret.x / right;
		ret.y = ret.y / right;
		return ret;
	}
};
struct Rect
{
	float size;
	Vec2 center, topLeft, bottomRight;

	Rect() {}
	void SetRect(Vec2 a_Center, float a_size)
	{
		size = a_size;
		SetCenter(a_Center);
	}

	void SetCenter(const Vec2& newCenter)
	{
		center = newCenter;
		topLeft = Vec2 (center.x - size / 2, center.y - size / 2);
		bottomRight = Vec2 (center.x + size / 2, center.y + size / 2);
	}
};