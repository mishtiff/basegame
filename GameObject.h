#pragma once

#include <iostream>
#include <allegro5/allegro5.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include "Globals.h"


class GameObject
{
private:
	int ID;
	int PlayerID;
	bool alive;
	bool collidable;
	Rect boundingBox;

protected:
	ALLEGRO_FONT *font18;
	Vec2 position;

	float velX;
	float velY;

	float destx;
	float desty;

	int dirX;
	int dirY;

	int boundX;
	int boundY;

	int shipCount;
	int shipKiller;

	int maxFrame;
	int curFrame;
	int frameCount;
	int frameDelay;
	int frameWidth;
	int frameHeight;
	int animationColumns;
	int animationDirection;

	ALLEGRO_BITMAP *image;
	ALLEGRO_COLOR BaseColor;

public:
	GameObject();
	void virtual Destroy();

	void Init(Vec2 pos, float velX, float velY, int dirX, int dirY, int boundX, int boundY);
	void virtual Update();
	void virtual Render();


	void SetPosition(Vec2 new_pos) {GameObject::position = position + new_pos; GameObject::setBoundingBox(position, 6);}
	void SetBoundX(int bx) {GameObject::boundX = bx;}
	void SetBoundY(int by) {GameObject::boundY = by;}
	void SetVelX(float x) {GameObject::velX = x;}
	void SetVelY(float y) {GameObject::velY = y;}
	void SetDestX(float x) {GameObject::destx = x;}
	void SetDestY(float y) {GameObject::desty = y;}

	Vec2 GetPosition() {return position;}
	int GetBoundX() {return boundX;}
	int GetBoundY() {return boundY;}
	float GetVelX() {return velX;}
	float GetVelY() {return velY;}
	float GetDestX() {return destx;}
	float GetDestY() {return desty;}
	
	int GetID() {return ID;}
	void SetID(int ID) {GameObject::ID = ID;}
	
	int GetPlayerID() {return PlayerID;}
	void SetPlayerID(int ID) {PlayerID = ID;}
	
	int GetShipCount() {return shipCount;}
	void SetShipCount(int count) {shipCount += count;}
	
	int GetShipKiller() {return shipKiller;}
	void SetShipKiller(int killer) {shipKiller = killer;}
	
	void SetColor(ALLEGRO_COLOR color) {BaseColor = color;}
	ALLEGRO_COLOR GetColor() {return BaseColor;}

	bool GetAlive() {return alive;}
	void SetAlive(bool alive) {GameObject::alive = alive;}

	bool GetCollidable() {return collidable;}
	void SetCollidable(bool collidable) {GameObject::collidable = collidable;}
		
	bool CheckCollisions(GameObject *otherObject);
	void virtual Collided(int objectID, Vec2 passed_position);
	bool Collidable();

	void setBoundingBox(Vec2 position, float a_size) {boundingBox.SetRect(position, a_size);}
	const Rect& GetBoundingBox() const { return boundingBox; }
    const Vec2& GetPosition() const { return boundingBox.center; }
};